#! /usr/bin/env python3
import gitlab
import os
import json

MANDATORY_ENV_VARS = ['GL_URL', 'GL_PRIVATE_TOKEN']
env = {}

for var in MANDATORY_ENV_VARS:
    if var not in os.environ:
        raise EnvironmentError("Failed because {} is not set.".format(var))
    env[var] = os.environ.get(var)


def lambda_handler(event, context):
    # Create connection to GL
    content=json.loads(event['body'])
    response =  {
        "statusCode": 200,
        "repo_url": content['project']['git_ssh_url'],
        "source_branch": content['object_attributes']['source_branch'],
        "target_branch": content['object_attributes']['target_branch']
    }
    print(response)
    return response

