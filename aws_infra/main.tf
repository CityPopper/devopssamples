terraform {

  required_version = "~> 1.1.9"
  
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.13"
    }
  }

  backend "s3" {
    bucket = "Replaced using backend-config option"
    key    = "Replaced using backend-config option"
  }
}

provider "aws" {
  region = "${var.aws_region}"
  default_tags {
    tags = { terraform : "true" }
  }
}
