variable "backend_bucket" {
  type        = string
  description = "The name of the bucket to store Terraform state."
}

variable "backend_key" {
  type        = string
  description = "The key in the s3 bucket."
}

variable "aws_region" {
  type        = string
  description = "AWS Region to deploy all resources."
}

variable "GL_URL" {
  type        = string
  description = "Base URL of GitLab."
}

variable "GL_PRIVATE_TOKEN" {
  type        = string
  description = "GL Private Token."
  sensitive   = true
}

