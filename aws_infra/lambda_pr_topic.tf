
resource "aws_lambda_function" "watch_for_prs" {  
  function_name = "watch_for_prs"
  filename      = "tmp/python_lambda.zip"
  runtime       = "python3.9"
  handler       = "main.lambda_handler"
  role          = aws_iam_role.lambda_exec.arn
  timeout       = 10
  environment {
    variables = {
      GL_URL="${var.GL_URL}"
      GL_PRIVATE_TOKEN="${var.GL_PRIVATE_TOKEN}"
    }
  }
}