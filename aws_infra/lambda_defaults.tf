# This is used to create empty lambdas which can have their own deployment pipeline
data "archive_file" "python-lambda-null-zip" {
  type = "zip"
  output_path = "tmp/${path.module}/python_lambda.zip"

  source {
    filename = "main.py"
    content = "def lambda_handler(event, context):\n  return event"
  }
}

resource "aws_iam_role" "lambda_exec" {
  name = "serverless_lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}